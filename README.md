# ROCKET #

### What is this repository for? ###

This is the sandbox for the programming of the sound installation 
[*The rocket that bumped off the ceiling*](https://hotel.schauspielhaus.at/events/max-windisch-spoerk-the-rocket-that-bumped-off-the-ceiling/)
, by Max Windisch-Spörk and Adrián Artacho.
Here we can work on the code back-and-forth, with some version control and the assurement, that we see the latest version :)

### Install ###

- Download the [**Rocket.amxdy**](https://bitbucket.org/AdrianArtacho/rocket/src/master/Rocket.amxd) M4L device from the repository.
- Put it inside your Ableton Set, in a New Track.

### Quick start ###

* Put files in the playlist (drop audio files onto the screen rectangle)
* Hit the *Random* button to play a different audio file each time.
* Enjoy!

## Mapping out audio & CC inside *Live* ##

The set up within Ableton Live typically involves 
5 differnt tracks, as shown in the picture below:

![repo:R107:bus](https://docs.google.com/drawings/d/e/2PACX-1vQ-RzkzvAzETcjLQoqWdVxSloNCxGo9VvQcK2xkD9p5Ux4tAJ-3SPiaKoM--VbKrbx3s-wLSFujlH7w/pub?w=927&h=161)


### *Launch* track ###
The first track (**Launch**) is there for the sole purpose of 'launching' 
specific tracks with a given scene. MIDI Out is routed to the **Rocket** track.

### *Rocket* track ###
The **Rocket** track is where the *Rocket.amxd* M4L device lives. 
It takes as input midinotes (from the **Launch** track or from elsewhere, 
that is why it needs to be set in **IN** ~~Auto~~.
It outputs audio (stereo) as well as 4 streams of midi CC messages:

* CC22 *Orientation*: (in a 4-speaker circle array, that is which speaker that comes from, more on that [later](#markdown-header-ceiling)
* CC23 *Midilight*: midinote to be sent to the PRESET track pointing at the the DMXIS device.
* CC24 *Background Reduction*: It translates the stored ~~suppression~~ reduction value (a percent 0-100%) to a midi value that can control thhe Gain knob of an utility object.
* CC25 *Opposite Orientation*: automatically calculated opposite speaker position. If set up with a value different than zero, you can set the specific position of the 'opposite rocket' audio to a fix one.


### *BumpOff* track ###
This track receives CC22-25 from the **Rocket** device, and spits it out as midi CC stream that can be directed to the **Ceiling** track,
where audio is spatialised. It is VERY IMPORTANT that the number of the [**BumpOff**](https://bitbucket.org/AdrianArtacho/bumpoff/src/master/) object be the same as the number in the corresponding 
**Rocket** device. Otherwise they cannot communicate with each other.


### *Ceiling* track ###
This track receives Audio from **Rocket** and CC from **BumpOff**.
The first device in the track is a [*META_Knob-A*](https://bitbucket.org/AdrianArtacho/meta_knob/src/master/) 
which converts the CC22-25
into mappable knobs.

HOW TO MAP A KNOB: 
* hit 'map' above the knob.
* hit the UI (knob, button, whatever) that you want to map it to.
* Enjoy!

--> IMPORTANT: The [*META_Knob*](https://bitbucket.org/AdrianArtacho/meta_knob/src/master/)
object has 8 little toggles on the right hand-side. 
That determines which of the default CC streams to listen to (CC22, CC23, CC24, CC25, CC104, CC105, CC106, CC107).

The CC22 (orientation) should be mapped to the *orientation* knob in the *surround panner* object.

The CC23 (midilights) is of no use here. You may leave it unmapped.

The CC24 (Background reduction) is meant to control the gains of **OTHER** rockets, so you don't
want to map the local rocket to the local *Utility gain*.
More on this in [How to rocket speaker placement](#markdown-header-How-to-rocket-speaker-placement).

### *MIDI-DMX* track ###

Here is where the [**Tesser_CC2note**](https://bitbucket.org/AdrianArtacho/bumpoff/src/master/) device lives.
The purpose of this track is therefore to receive midi CC from **Rocket** (via the [**BumpOff**](https://bitbucket.org/AdrianArtacho/bumpoff/src/master/)
device in the *BumpOff* track) and convert it with the [**Tesser_CC2note**](https://bitbucket.org/AdrianArtacho/bumpoff/src/master/) 
into a midinote that will be forwarded to the *PRESET* track. This *PRESET* track points at the midi channel 16 of the DMXIS plugin.

## UI Controls ##

![repo:R107](https://docs.google.com/drawings/d/e/2PACX-1vS9oj-XwCNOsqhJo78plP7N2p13BIQZYyAkQAj9v3gjH4QGQAx46ZfCZxgxU2_ebiBYbw1nEPsFfEDP/pub?w=923&h=233)

##### Select Random #####
Self explanatory.

##### Field ID #####
Number of the Clip being played.

##### Avoid clip repeats #####
Self explanatory.

##### Avoid orientation repetition #####
Self explanatory.

##### Auto/Trigger Mode #####
When in *AUTO* Mode, after a clip, another one will follow suit, selected randomly.
When in *Trigger* Mode, clips can only be launched by sending midi notes* to the Track Input.

1) Midinote 1 will trigger Clip 2, midinote 2 clip 2... etc. For any midinote number higher than the amount of clips in the rocket, a random clip will be launched.

## How to program rocket behaviour ##

There are two ways
to set the programmed behaviour of the rocket:

* 1) Directly editing the textfile.
* 2) Using the UI elements.

For the first procedure, click on 
*Pogram*, search for the right column, and change the value right after
the name of the parameter (E.g. *midilight* **4**). After performing a text edit:

- Close window
- Validate edit
- Click SAVE

In order to use the UI objects, simply edit them
while the corresponding audio clip IS PLAYING:

- Launch clip associated with program (e.g. Clip 4) using the field ID, for example)
- Move knob / write on numbox of the program value you wish to edit (e.g. midilight)
- Click SAVE

**IMPORTANT!** Sometimes, the UI programming mode won't work on startup.
So, even if you wnanna use this procedure, it is best to make a 
direct text edit using the first procedure before.

The programs will only be commited to the set memory once the button **SAVE**
is clicked, so make sure you do that at least once at the end of a programming session.



### Import / Export programs ###

You may export the program as a *.txt*
file using the corresponding **export** button.
This is useful if the device patch is itself 
going to be edited, in which case all the instances program info will be lost.

In order to restore an instance program from the saved *.txt* file, 
use the **import** button.


## How to set speaker orientation  #

(incl. opposite)


In the **Rocket** device, you can either set in via text 
(hit on **program**), setting a value 1-4 after *Orientation*,
or you can use the first knob while the track you wanna use is playing.

### Opposite orientation ###

In order to have OPPOSITE orientation working, 
make a NEW TRACK, put in the **MIDI CHANGE** object [which you can download here](https://drive.google.com/file/d/1nRbHtHC-R11ly3a-vIHErooWt9889zd3/view?usp=sharing)
to convert CC25 into any of the other CC numbers that **META_knob** reads by default (CC104, 105, 106, 107)
and point the track to the **Ceiling** track of a different rocket.
There, use whichever CC you chose to point the 'opposite rotation' to (say CC104, for example) 
and map that knob to the *rotation* knob in the surround panner.

REMEMBER! if you do this, turn off the the first toggle in the *META_Knob* device, to disable the local rotation CC stream
from interfering!



## How to triggers clips based on floor input ##
Check out the [**Floor** README](https://bitbucket.org/AdrianArtacho/floor/src/master/README.md) for that.

## How to do scenes? ##

A *SCENE* is shorthand for a program that includes
launching an audio clip AND a light preset 
(or a sequence thereof) together.

In order to set scenes using a **Rocket**,
imply set the sound clips in the Rocket device, each 
[programmed](#markdown-header-how-to-program-rocket-behaviour) with a *Midilight* number. 
That will go out as CC to the **BumpOff** track.
There, use a [**CC2note**](https://bitbucket.org/AdrianArtacho/tesser_cc2note) object (Copy & Paste from *MIDI-DMX* track)
to convert CC25 to a midinote.
If you need to trigger multiple midinotes for the scenes, add a 
[**Tesser_Clips**](https://bitbucket.org/AdrianArtacho/tesser_clips/src/master/)
M4L object (you may Copy & Paste from the tracks right to the FLOOR track: *routeClips:(IN)ACTIVE*)
to point to a slot in the PRESET track with many midi notes.

Example: *Midilight (CC25)* **value 5** will be converted (CC2note) to **midinote 5**, which will 
launch **slot 5** (+ 5 offset, so slot 10) in the **Presets (in)active** track, 
which has the sequence of midinote you want to be sent to the *PRESET* track pointing to the DMXIS track.

## How do I edit/work on the patch? ###

If you have the device in *Ableton* already, then you can click on the right-up button of the device itself to edit it (MaxMSP is shipped with Ableton anyway).
Once you are in the Max workspace, here are some hints to get you started:

* In MaxMSP, right button on an object (context menu) shows you the help file, with examples, etc. The workflow should be visually somewhat self explanatory.
* Play around it as much as you want. Commit a change when you think you have something nice going on.

### How do I get the project files? ###

* Clone the repository (downloads it to your laptop)
* Unzip it
* Open the MaxMSP project (*Rocket.maxproj*)
* Open the patch (*Rocket.maxpat*)



### How do I commit a change to the repository? ###

* There's plenty of documentation about GIT around there, but I suggest you jsut install **SOURCETREE** application directly from *bitbucket* and use it to clone and commit changes to the repository.


# To-Do #

* closebang?
* only stores in coll after validate once ??
* fade out (device wide) for audio AND for Background Reduction

