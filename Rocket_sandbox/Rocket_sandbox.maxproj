{
	"name" : "Rocket_sandbox",
	"version" : 1,
	"creationdate" : 3718193022,
	"modificationdate" : 3722164294,
	"viewrect" : [ 25.0, 104.0, 300.0, 500.0 ],
	"autoorganize" : 1,
	"hideprojectwindow" : 0,
	"showdependencies" : 1,
	"autolocalize" : 0,
	"contents" : 	{
		"patchers" : 		{
			"Rocket_sandbox.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1,
				"toplevel" : 1
			}
,
			"Rocket.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}

		}
,
		"media" : 		{
			"BOUNCE 0002 [norm].aiff" : 			{
				"kind" : "audiofile",
				"local" : 1
			}
,
			"Communication_Drone.wav" : 			{
				"kind" : "audiofile",
				"local" : 1
			}
,
			"Slumber_E_Sub_Bass.wav" : 			{
				"kind" : "audiofile",
				"local" : 1
			}
,
			"SlowMachine_Dark_Drone.wav" : 			{
				"kind" : "audiofile",
				"local" : 1
			}
,
			"Distant_Spaces_Atmos.wav" : 			{
				"kind" : "audiofile",
				"local" : 1
			}

		}
,
		"data" : 		{
			"Programming.txt" : 			{
				"kind" : "textfile",
				"local" : 1
			}

		}

	}
,
	"layout" : 	{

	}
,
	"searchpath" : 	{

	}
,
	"detailsvisible" : 0,
	"amxdtype" : 1768515945,
	"readonly" : 0,
	"devpathtype" : 0,
	"devpath" : ".",
	"sortmode" : 0,
	"viewmode" : 0
}
